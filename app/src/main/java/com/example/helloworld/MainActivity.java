package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void breakBad(View view) {
        ImageView image = findViewById(R.id.walterWhite);
        TextView text = findViewById(R.id.jesseCook);
        if (image.getVisibility() == View.VISIBLE) {
            image.setVisibility(View.INVISIBLE);
            text.setVisibility(View.INVISIBLE);
        } else {
            image.setVisibility(View.VISIBLE);
            text.setVisibility(View.VISIBLE);
        }
    }

    public void forget(View view){
        ImageView image = findViewById(R.id.padraig);
        TextView text = findViewById(R.id.etherNet);
        if (image.getVisibility() == View.VISIBLE) {
            image.setVisibility(View.INVISIBLE);
            text.setVisibility(View.INVISIBLE);
        } else {
            image.setVisibility(View.VISIBLE);
            text.setVisibility(View.VISIBLE);
        }
    }
}